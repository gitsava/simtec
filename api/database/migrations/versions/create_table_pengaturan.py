"""create table pengaturan

Revision ID: 5effa5f6262e
Revises: 1f785e2126be
Create Date: 2021-12-09 03:02:20.428102

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '5effa5f6262e'
down_revision = '1f785e2126be'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('pengaturan',
        sa.Column('nm_app', sa.String(100), primary_key=True, nullable=True),
        sa.Column('no_tlp', sa.String(20), nullable=False),
        sa.Column('alamat', sa.TEXT()),
        sa.Column('logo', sa.String(50), nullable=True),
        sa.Column('jam_masuk', sa.String(25), nullable=True),
        sa.Column('jam_keluar', sa.String(25), nullable=True)
    )

def downgrade():
    op.drop_table('pengaturan')

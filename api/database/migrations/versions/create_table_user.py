"""create table user

Revision ID: 1f785e2126be
Revises: 11baf01e2bf1
Create Date: 2021-12-09 03:01:53.731171

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.sql.schema import Column, ForeignKey


# revision identifiers, used by Alembic.
revision = '1f785e2126be'
down_revision = '11baf01e2bf1'
branch_labels = None
depends_on = None

def upgrade():
    op.create_table('user',
        sa.Column('id', sa.Integer, primary_key=True, autoincrement=True, nullable=True),
        sa.Column('username', sa.String(30), nullable=False),
        sa.Column('paswword', sa.String(45), nullable=False),
        sa.Column('email', sa.String(45), unique=True, nullable=False),
        sa.Column('level', sa.Integer, nullable=False),
        sa.Column('blokir', sa.Enum('y','n')),
        sa.Column('nik_user', sa.String(30), nullable=True),
        sa.Column('id_bagian', sa.Integer, nullable=True)
    )

    #op.create_foreign_key("fk_user_nik", "user", "nik", ["nik_user"], ["nik"])
    #op.create_foreign_key("fk_user_bagian", "user","bagian", ["id_bagian"], ["id"])

def downgrade():
    op.drop_table('user')

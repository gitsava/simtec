"""create table pegawai

Revision ID: ba98e4717d34
Revises: 55ca3934ac00
Create Date: 2021-12-09 03:00:30.683377

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'ba98e4717d34'
down_revision = '55ca3934ac00'
branch_labels = None
depends_on = None

def upgrade():
    op.create_table('pegawai',
        sa.Column('id', sa.Integer, primary_key=True, autoincrement=True, nullable=True),
        sa.Column('nik', sa.String(30), nullable=True),
        sa.Column('nama', sa.String(45), nullable=False),
        sa.Column('no_tlp', sa.String(20), nullable=True),
        sa.Column('alamat', sa.String(255), nullable=True),
        sa.Column('tgl_lahir', sa.Date),
        sa.Column('tgl_masuk_kerja', sa.Date)
    )

def downgrade():
    op.drop_table('pegawai')

# MVC-Fastapi API
FastApi CRUD MYSQL with concept MVC

#### 1. Saya menggunakan Anaconda
#### 2. Buat lingkugan kerja (environment) Anaconda

Di direktori repo utama
```shell
    conda env create -f api/envs/environment.yml
    conda activate simtec
    pip freeze > api/envs/requirements.txt

```

## Menginstal dengan semua dependensi pada FastApi
```shell
pip install "fastapi[all]"
```

## Menginstall dependensi secara terpisah
Lihat Referensi klik di <a href="https://fastapi.tiangolo.com/"> ini </a>
```
pip install fastapi
pip install "uvicorn[standard]"
pip install SQLAlchemy
pip install alembic
pip install PyMySQL
pip install pysqlite3
pip install Faker
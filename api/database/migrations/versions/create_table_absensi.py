"""create table absensi

Revision ID: cebfd9a46813
Revises: 5effa5f6262e
Create Date: 2021-12-09 03:02:41.138743

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'cebfd9a46813'
down_revision = '5effa5f6262e'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('absensi',
        sa.Column('id', sa.Integer, primary_key=True, autoincrement=True, nullable=True),
        sa.Column('jam_absen', sa.String(25), nullable=False),
        sa.Column('tgl_absen', sa.String(25), nullable=False),
        sa.Column('kd_absen', sa.Enum('1','2','3','4')),
        sa.Column('jam_keluar', sa.String(25), nullable=True),
        sa.Column('jam_lembur', sa.String(25), nullable=True),
        sa.Column('id_user', sa.Integer(3), nullable=True)
    )

    op.create_foreign_key(
        "fk_absensi_user", "absensi",
        "user", ["id_user"], ["id"])

def downgrade():
    op.drop_table('absensi')

# SIMTEC
SIMTEC (Sistem Teknologi) fokus science

# Migrasi menggunakan Alembic
```
pip install alembic
```

## Create Table
```bash
cd path/to/alembic.ini
alembic revision -m "create table tb_nama"
```

## Run migrations data
```bash
cd path/to/alembic.ini
alembic upgrade head
```

## Run Project SIMTEC
```bash
uvicorn main:app --reload
```
"""create table bagian

Revision ID: 11baf01e2bf1
Revises: ba98e4717d34
Create Date: 2021-12-09 03:01:23.731540

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '11baf01e2bf1'
down_revision = 'ba98e4717d34'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('bagian',
        sa.Column('id',sa.Integer, primary_key=True, autoincrement=True, nullable=True),
        sa.Column('nama',sa.String(100),nullable=False),
        sa.Column('status_aktif',sa.Enum('0','1'))
    )

def downgrade():
    op.drop_table('bagian')
